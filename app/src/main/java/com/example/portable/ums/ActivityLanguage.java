package com.example.portable.ums;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.LocalizationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.APP_PREF;
import static com.example.portable.ums.Util.updateResources;

public class ActivityLanguage extends LocalizationActivity {

    private TextView mRussian;
    private TextView mUzbek;
    /*private SharedPreferences mSharedPreferences;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        setTitle(getString(R.string.language));

        mRussian = (TextView) findViewById(R.id.russian);
        mUzbek = (TextView) findViewById(R.id.uzbek);

        /*mSharedPreferences = getSharedPreferences(APP_PREF, MODE_PRIVATE);*/

        /*if (LocaleHelper.getLanguage(this).equals("en")) {
            setColor(mRussian);
        } else {
            setColor(mUzbek);
        }*/

        if (getLanguage().equals("en")) {
            mRussian.setTextColor(getColor());
        } else {
            mUzbek.setTextColor(getColor());
        }
    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }*/

    @OnClick({R.id.russian, R.id.uzbek})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.russian:
                updateViews("en");
                break;
            case R.id.uzbek:
                updateViews("uz");
                break;
        }
    }

    /*public void setLanguage(String lang) {
        mSharedPreferences  = getSharedPreferences(APP_PREF, MODE_PRIVATE);

        mSharedPreferences.edit()
                .putString("lang", lang)
                .commit();

        updateResources(getApplicationContext(), lang);
    }*/

    private void updateViews(String languageCode) {
        /*LocaleHelper.setLocale(this, languageCode);*/
        if (languageCode.equals("en")) {
/*            mRussian.setTextColor(getColor());
            mUzbek.setTextColor(Color.BLACK);*/
            setLanguage("en");
        } else {
/*            mUzbek.setTextColor(getColor());
            mRussian.setTextColor(Color.BLACK);*/
            setLanguage("uz");
        }
    }

    public int getColor() {
        if (Build.VERSION.SDK_INT >= 23) {
            return getColor(R.color.colorPrimary);
        } else {
            return getResources().getColor(R.color.colorPrimary);
        }
    }
}
