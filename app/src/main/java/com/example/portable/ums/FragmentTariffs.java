package com.example.portable.ums;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.getPermission;

public class FragmentTariffs extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tariffs, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick({R.id.tariff_start, R.id.tariff_optima_333, R.id.tariff_ultra, R.id.tariff_555, R.id.tariff_maxi_new, R.id.tariff_perfect, R.id.tariff_777})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.tariff_start:
                ussd = getString(R.string.ussd_tariff_start);
                break;
            case R.id.tariff_optima_333:
                ussd = getString(R.string.ussd_tariff_optima_333);
                break;
            case R.id.tariff_ultra:
                ussd = getString(R.string.ussd_tariff_ultra);
                break;
            case R.id.tariff_555:
                ussd = getString(R.string.ussd_tariff_555);
                break;
            case R.id.tariff_maxi_new:
                ussd = getString(R.string.ussd_tariff_maxi_new);
                break;
            case R.id.tariff_perfect:
                ussd = getString(R.string.ussd_tariff_perfect);
                break;
            case R.id.tariff_777:
                ussd = getString(R.string.ussd_tariff_777);
                break;
        }

        getPermission(getActivity(), Manifest.permission.CALL_PHONE, ussd);
    }
}
