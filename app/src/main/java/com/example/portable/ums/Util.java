package com.example.portable.ums;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.CompositePermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener;

import java.util.Locale;

/**
 * Created by Portable on 10.02.2017.
 */

public class Util {
    public static final String APP_PREF = "data_store";

    public static void updateResources(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();

        if (Build.VERSION.SDK_INT >= 17) {
            configuration.setLocale(locale);
            context.createConfigurationContext(configuration);
        } else {
            configuration.locale = locale;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
    }

    public static void getPermission(Activity activity, String permission, String ussd) {
        ViewGroup viewGroup = (ViewGroup) activity.findViewById(android.R.id.content);

        PermissionListener permissionListener = new CompositePermissionListener(new SamplePermissionListener(activity, ussd),
                SnackbarOnDeniedPermissionListener.Builder
                        .with(viewGroup, activity.getString(R.string.call_permission_snackbar))
                        .withOpenSettingsButton(R.string.settings)
                        .withCallback(new Snackbar.Callback() {
                            @Override
                            public void onShown(Snackbar sb) {
                                super.onShown(sb);
                            }

                            @Override
                            public void onDismissed(Snackbar transientBottomBar, int event) {
                                super.onDismissed(transientBottomBar, event);
                            }
                        }).build());

        Dexter.withActivity(activity)
                .withPermission(permission)
                .withListener(permissionListener)
                .check();
    }

    public static void showPermissionRationale(final PermissionToken token, final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.call_permission_dialog_title))
                .setMessage(activity.getString(R.string.call_permission_dialog_message))
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        token.cancelPermissionRequest();
                    }
                }).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                token.continuePermissionRequest();
            }
        }).setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                token.cancelPermissionRequest();
            }
        }).show();
    }

    /*public void showPermissionGranted(String permission, Activity activity) {

    }

    public void showPermissionDenied(String permission, boolean isPermanentlyDenied, Activity activity) {

    }*/
}
