package com.example.portable.ums;

import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.getPermission;

public class ActivityServices extends LocalizationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        setTitle(getString(R.string.services));

        ButterKnife.bind(this);
    }

    @OnClick({R.id.service1_activate, R.id.service2_activate, R.id.service2_deactivate,
            R.id.service3_activate, R.id.service3_deactivate, R.id.service4_activate,
            R.id.service4_deactivate, R.id.service5_activate, R.id.service5_deactivate,
            R.id.service5_check_status})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.service1_activate:
                ussd = getString(R.string.ussd_obeshanniy_paltej);
                break;
            case R.id.service2_activate:
                ussd = getString(R.string.ussd_ojedaniya_uderjaniya_vizova_activate);
                break;
            case R.id.service2_deactivate:
                ussd = getString(R.string.ussd_ojedaniya_uderjaniya_vizova_deactivate);
                break;
            case R.id.service3_activate:
                ussd = getString(R.string.ussd_vam_zvonili_activate);
                break;
            case R.id.service3_deactivate:
                ussd = getString(R.string.ussd_vam_zvonili_deactivate);
                break;
            case R.id.service4_activate:
                ussd = getString(R.string.ussd_antiaon_activate);
                break;
            case R.id.service4_deactivate:
                ussd = getString(R.string.ussd_antiaon_deactivate);
                break;
            case R.id.service5_activate:
                ussd = getString(R.string.ussd_4g_activate);
                break;
            case R.id.service5_deactivate:
                ussd = getString(R.string.ussd_4g_deactivate);
                break;
            case R.id.service5_check_status:
                ussd = getString(R.string.ussd_4g_check_status);
                break;
        }

        getPermission(this, Manifest.permission.CALL_PHONE, ussd);
    }
}
