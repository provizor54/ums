package com.example.portable.ums;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.example.portable.ums.Util.getPermission;

/**
 * Created by Portable on 07.02.2017.
 */

public class FragmentBalance extends Fragment {

    private Unbinder mUnbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_balance, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    @OnClick({R.id.balance, R.id.last_payment, R.id.my_expense, R.id.my_number, R.id.all_my_numbers})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.balance:
                ussd = getString(R.string.check_balance);
                break;
            case R.id.last_payment:
                ussd = getString(R.string.ussd_last_payment);
                break;
            case R.id.my_expense:
                ussd = getString(R.string.ussd_my_expense);
                break;
            case R.id.my_number:
                ussd = getString(R.string.ussd_my_number);
                break;
            case R.id.all_my_numbers:
                ussd = getString(R.string.ussd_all_my_numbers);
                break;
        }

        getPermission(getActivity(), Manifest.permission.CALL_PHONE, ussd);
    }
}
