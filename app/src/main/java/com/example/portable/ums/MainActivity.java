package com.example.portable.ums;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

public class MainActivity extends LocalizationActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private Drawer mDrawer;
    private int mTabIndex;
    public static final String TAG = "myLogs";
    private NavigationView mNavigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        setTitle(getString(R.string.label));

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(mViewPager);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mViewPager);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        /*mNavigationView.setCheckedItem();*/

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCheckedItem(mTabLayout.getSelectedTabPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        setCheckedItem(mTabLayout.getSelectedTabPosition());
    }

/*@Override
    protected void onPause() {
        super.onStop();

        mTabIndex = mTabLayout.getSelectedTabPosition();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        mViewPager.setCurrentItem(mTabIndex);
    }*/

    private void initDrawer() {
        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.background)
                .build();

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withIdentifier(1)
                                .withName(getString(R.string.balance)),
                        new PrimaryDrawerItem()
                                .withIdentifier(2)
                                .withName(getString(R.string.internet_packages)),
                        new PrimaryDrawerItem()
                                .withIdentifier(3)
                                .withName(getString(R.string.tariffs)),
                        new PrimaryDrawerItem()
                                .withIdentifier(4)
                                .withName(getString(R.string.services)),
                        new PrimaryDrawerItem()
                                .withIdentifier(5)
                                .withName(getString(R.string.minutes_sms_packages)),
                        new PrimaryDrawerItem()
                                .withIdentifier(6)
                                .withName(getString(R.string.news)),
                        new PrimaryDrawerItem()
                                .withIdentifier(7)
                                .withName(getString(R.string.settings))
                        )
                .withActionBarDrawerToggleAnimated(true)
                .withToolbar(mToolbar)
                .build();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new FragmentBalance(), getString(R.string.balance));
        adapter.addFragment(new FragmentInternetPackages(), getString(R.string.internet_packages));
        adapter.addFragment(new FragmentTariffs(), getString(R.string.tariffs));

        viewPager.setAdapter(adapter);
    }

    /*@Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.balance:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.internet_packages:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.tariffs:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.services:
                startActivity(new Intent(this, ActivityServices.class));
                break;
            case R.id.minute_packages:
                startActivity(new Intent(this, ActivityMinutes.class));
                break;
            case R.id.sms_packages:
                startActivity(new Intent(this, ActivitySMS.class));
                break;
            case R.id.language:
                startActivity(new Intent(this, ActivityLanguage.class));
                break;
            case R.id.news:
                startActivity(new Intent(this, ActivityNews.class));
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setCheckedItem(int position) {
        switch (position) {
            case 0:
                mNavigationView.setCheckedItem(R.id.balance);
                break;
            case 1:
                mNavigationView.setCheckedItem(R.id.internet_packages);
                break;
            case 2:
                mNavigationView.setCheckedItem(R.id.tariffs);
                break;
        }
    }
}
