package com.example.portable.ums;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

/**
 * Created by Portable on 26.02.2017.
 */

public class SamplePermissionListener implements PermissionListener {
    private final Activity activity;
    private String ussd;

    public SamplePermissionListener(Activity activity, String ussd) {
        this.activity = activity;
        this.ussd = ussd;
    }

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {
        StringBuilder ussdBuilder = new StringBuilder();

        for (int i = 0; i < ussd.length(); i++) {
            if (ussd.charAt(i) == '*') {
                ussdBuilder.append(Uri.encode("*"));
            } else if (ussd.charAt(i) == '#') {
                ussdBuilder.append(Uri.encode("#"));
            } else {
                ussdBuilder.append(ussd.charAt(i));
            }
        }

        ussdBuilder.append(Uri.encode("#"));

        /*Log.d("myLogs", ussdBuilder.toString());*/

        activity.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdBuilder.toString())));
    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {

    }

    @Override
    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
        Util.showPermissionRationale(token, activity);
    }
}
