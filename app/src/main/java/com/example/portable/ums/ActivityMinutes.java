package com.example.portable.ums;

import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.akexorcist.localizationactivity.LocalizationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.getPermission;

public class ActivityMinutes extends LocalizationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minutes);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        setTitle(getString(R.string.minute_packages));

        ButterKnife.bind(this);
    }

    @OnClick({R.id.minutes_120, R.id.minutes_180, R.id.minutes_300,})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.minutes_120:
                ussd = getString(R.string.ussd_minutes_120);
                break;
            case R.id.minutes_180:
                ussd = getString(R.string.ussd_minutes_180);
                break;
            case R.id.minutes_300:
                ussd = getString(R.string.ussd_minutes_300);
                break;
        }

        getPermission(this, Manifest.permission.CALL_PHONE, ussd);
    }
}
