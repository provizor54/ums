package com.example.portable.ums;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.akexorcist.localizationactivity.LocalizationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.getPermission;

public class ActivityNightDriveInternet extends LocalizationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_night_drive_internet);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        setTitle(getString(R.string.night_drive));
    }

    @OnClick({R.id.night_drive_internet1, R.id.night_drive_internet2, R.id.night_drive_internet3})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.night_drive_internet1:
                ussd = getString(R.string.ussd_drive_internet1);
                break;
            case R.id.night_drive_internet2:
                ussd = getString(R.string.ussd_drive_internet2);
                break;
            case R.id.night_drive_internet3:
                ussd = getString(R.string.ussd_drive_internet3);
                break;
        }

        getPermission(this, Manifest.permission.CALL_PHONE, ussd);
    }
}
