package com.example.portable.ums;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ActivityNews extends AppCompatActivity {

    private UltimateRecyclerView mUltimateRecyclerView;
    private String mAccessToken;
    private boolean isLoading = false;
    private Handler mHandler;
    private JSONObject mPosts;
    private ProgressBar mProgressBar;
    private RelativeLayout mInternetConnectionLayout;
    private OkHttpClient mClient;
    private RecyclerViewAdapter mAdapter;
    /*private LinearLayout mRetry;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        setTitle(getString(R.string.news));
        ButterKnife.bind(this);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar2);
        mInternetConnectionLayout = (RelativeLayout) findViewById(R.id.internet_connection);
        /*mRetry = (LinearLayout) findViewById(R.id.retry);*/
        mAdapter = new RecyclerViewAdapter(ActivityNews.this);
        mUltimateRecyclerView = (UltimateRecyclerView) findViewById(R.id.rv);
        mUltimateRecyclerView.setLayoutManager(new LinearLayoutManager(ActivityNews.this));
        mUltimateRecyclerView.setHasFixedSize(false);
        mUltimateRecyclerView.setLoadMoreView(R.layout.view_progress);

        request();
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @OnClick(R.id.retry)
    public void onClick() {
        mInternetConnectionLayout.setVisibility(GONE);
        mUltimateRecyclerView.setVisibility(View.GONE);
        mProgressBar.setVisibility(VISIBLE);
        request();
    }

    private void request() {
        if (!isOnline()) {
            mUltimateRecyclerView.setVisibility(GONE);
            mProgressBar.setVisibility(GONE);
            mInternetConnectionLayout.setVisibility(VISIBLE);

            return;
        }

        Request request = new Request.Builder()
                .url("")
                .build();

        mClient = new OkHttpClient();

        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String resp = response.body().string();

                mAccessToken = resp.substring(resp.indexOf('=') + 1);

                final AccessToken accessToken = new AccessToken(mAccessToken, "", "", null, null, null, null, null);

                new GraphRequest(accessToken, "UniversalMobileSystems.uz?fields=posts.limit(50){message,full_picture}", null, HttpMethod.GET, new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(final GraphResponse response) {
                        //Log.d("myLogs", response.toString());
                        try {
                            mPosts = response.getJSONObject().getJSONObject("posts");
                            JSONArray data = mPosts.getJSONArray("data");

                            mAdapter.addData(data);
                            mUltimateRecyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            mProgressBar.setVisibility(GONE);
                            mInternetConnectionLayout.setVisibility(GONE);
                            mUltimateRecyclerView.setVisibility(View.VISIBLE);
                            mUltimateRecyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
                                @Override
                                public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                                    if (!isLoading) {
                                        isLoading = true;
                                        try {
                                            String url = mPosts.getJSONObject("paging").getString("next");
                                            url = url.substring(url.indexOf('?') + 1);

                                            new GraphRequest(accessToken, "UniversalMobileSystems.uz/posts?" + url, null, HttpMethod.GET, new GraphRequest.Callback() {
                                                @Override
                                                public void onCompleted(final GraphResponse response) {
                                                    mPosts = response.getJSONObject();
                                                    Log.d("myTags", response.toString());
                                                    try {
                                                        mAdapter.addData(response.getJSONObject().getJSONArray("data"));
                                                        mAdapter.notifyDataSetChanged();
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    isLoading = false;
                                                }
                                            }).executeAsync();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            isLoading = false;
                                        }
                                    }
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).executeAsync();
            }
        });
    }
}
