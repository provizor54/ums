package com.example.portable.ums;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Portable on 15.02.2017.
 */

public class RecyclerViewAdapter extends UltimateViewAdapter {

    private JSONArray list;
    private Context mContext;

    public RecyclerViewAdapter(Context context) {
        mContext = context;
        list = new JSONArray();
    }

    public RecyclerViewAdapter(Context context, JSONArray list) {
        this.list = list;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder newFooterHolder(View view) {
        return new UltimateRecyclerviewViewHolder<>(view);
    }

    @Override
    public RecyclerView.ViewHolder newHeaderHolder(View view) {
        return new UltimateRecyclerviewViewHolder<>(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);

        ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public int getAdapterItemCount() {
        return list.length();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position < getItemCount() && (customHeaderView != null ? position <= list.length() : position < list.length()) && (customHeaderView != null ? position > 0 : true)) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            int pos = customHeaderView != null ? position - 1 : position;
            viewHolder.tv.setMaxLines(7);
            viewHolder.viewMore.setVisibility(View.VISIBLE);
            try {
                JSONObject data = list.getJSONObject(pos);
                viewHolder.tv.setText(data.getString("message"));

                if (data.has("full_picture")) {
                    Picasso.with(mContext)
                            .load(data.getString("full_picture"))
                            .into(viewHolder.image);
                    /*Glide.with(mContext)
                            .load(data.getString("full_picture"))
                            .into(viewHolder.image);*/

                    viewHolder.viewMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            viewHolder.tv.setMaxLines(Integer.MAX_VALUE);
                            viewHolder.viewMore.setVisibility(View.GONE);
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        /*View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;*/

        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
    }

    public void addData(JSONArray list) throws JSONException {
        //JSONArray array = new JSONArray();
        for (int i = 0; i < list.length(); i++) {
            this.list.put(list.getJSONObject(i));
        }
    }

    private class ViewHolder extends UltimateRecyclerviewViewHolder {
        private TextView tv;
        private ImageView image;
        private TextView viewMore;

        public ViewHolder(View itemView) {
            super(itemView);

            tv = (TextView) itemView.findViewById(R.id.text);
            image = (ImageView) itemView.findViewById(R.id.image);
            viewMore = (TextView) itemView.findViewById(R.id.view_more);
        }
    }
}
