package com.example.portable.ums;

import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.akexorcist.localizationactivity.LocalizationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.getPermission;

public class ActivitySMS extends LocalizationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        setTitle(getString(R.string.sms_packages));

        ButterKnife.bind(this);
    }

    @OnClick({R.id.sms_100, R.id.sms_300})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.sms_100:
                ussd = getString(R.string.ussd_sms_100);
                break;
            case R.id.sms_300:
                ussd = getString(R.string.ussd_sms_300);
                break;
        }

        getPermission(this, Manifest.permission.CALL_PHONE, ussd);
    }
}
