package com.example.portable.ums;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.akexorcist.localizationactivity.LocalizationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.portable.ums.Util.getPermission;

public class ActivityInternetPackagesDescribe extends LocalizationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet_packages_describe);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        setTitle(getString(R.string.internet_packages));
    }

    @OnClick({R.id.internet_package1, R.id.internet_package2, R.id.internet_package3, R.id.internet_package4, R.id.internet_package5, R.id.internet_package6, R.id.internet_package7})
    public void onClick(View view) {
        String ussd = null;

        switch (view.getId()) {
            case R.id.internet_package1:
                ussd = getString(R.string.ussd_internet_package1);
                break;
            case R.id.internet_package2:
                ussd = getString(R.string.ussd_internet_package2);
                break;
            case R.id.internet_package3:
                ussd = getString(R.string.ussd_internet_package3);
                break;
            case R.id.internet_package4:
                ussd = getString(R.string.ussd_internet_package4);
                break;
            case R.id.internet_package5:
                ussd = getString(R.string.ussd_internet_package5);
                break;
            case R.id.internet_package6:
                ussd = getString(R.string.ussd_internet_package6);
                break;
            case R.id.internet_package7:
                ussd = getString(R.string.ussd_internet_package7);
                break;
        }

        getPermission(this, Manifest.permission.CALL_PHONE, ussd);
    }
}
