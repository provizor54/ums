package com.example.portable.ums;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Portable on 09.02.2017.
 */

public class FragmentInternetPackages extends Fragment {

    private Unbinder mUnbinder;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_internet_packages, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    @OnClick(R.id.packages)
    public void onClickPackages() {
        startActivity(new Intent(getContext(), ActivityInternetPackagesDescribe.class));
    }

    @OnClick(R.id.night_packages)
    public void onClickNightPackages() {
        startActivity(new Intent(getContext(), ActivityNightInternet.class));
    }

    @OnClick(R.id.night_drive_packages)
    public void onClickNightDrivePackages() {
        startActivity(new Intent(getContext(), ActivityNightDriveInternet.class));
    }

    @OnClick(R.id.mini_internet_packages)
    public void onClickMiniInternetPackages() {
        startActivity(new Intent(getContext(), ActivityMiniInternet.class));
    }
}
